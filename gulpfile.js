var gulp           = require('gulp'),
		gutil          = require('gulp-util' ),
		sass           = require('gulp-sass'),
		browserSync    = require('browser-sync'),
		concat         = require('gulp-concat'),
		uglify         = require('gulp-uglify'),
		cleanCSS       = require('gulp-clean-css'),
		rename         = require('gulp-rename'),
		del            = require('del'),
		imagemin       = require('gulp-imagemin'),
		cache          = require('gulp-cache'),
		autoprefixer   = require('gulp-autoprefixer'),
		ftp            = require('vinyl-ftp'),
		notify         = require("gulp-notify"),
		fileinclude    = require('gulp-file-include');

// Normal
gulp.task('normal-js', function() {
	return gulp.src([
		'app/blocks/page-main/page-main.js',
		'app/blocks/page-catalog/page-catalog.js',
		'app/blocks/page-card/page-card.js',
		'app/blocks/page-login/page-login.js',
		'app/blocks/page-event-statistic/page-event-statistic.js',
		'app/blocks/page-visitor-account/page-visitor-account.js',
		'app/blocks/page-create-new-event-step1/page-create-new-event-step1.js',
		'app/blocks/page-create-new-event-step2/page-create-new-event-step2.js',
		'app/blocks/page-create-new-event-step3_preview/page-create-new-event-step3_preview.js',
		'app/blocks/page-verification/page-verification.js',
		'app/blocks/page-ticket-check/page-ticket-check.js',
		'app/blocks/page-personal-data/page-personal-data.js',
		'app/blocks/page-training-poster_1/page-training-poster_1.js',
		'app/blocks/page-training-poster_3/page-training-poster_3.js',
		'app/blocks/page-newsletter/page-newsletter.js',
		'app/blocks/page-seats/page-seats.js',
		'app/js/common.js'
	])
	.pipe(concat('common.js'))
	.pipe(gulp.dest('app/normal/js'))
	.pipe(browserSync.reload({stream: true}));
});

gulp.task('normal-libs-js', function() {
	return gulp.src([
		'app/libs/jquery/dist/jquery-3.3.1.min.js',
		'app/libs/bootstrap/dist/popper.min.js',
		'app/libs/bootstrap/dist/bootstrap.js',
		'app/libs/slick/dist/slick.min.js',
		'app/libs/select2/dist/select2.min.js',
		'app/libs/nouislider/dist/nouislider.min.js',
		'app/libs/datepicker-ui/dist/jquery-ui.min.js',
		'app/libs/datepicker-ui/dist/jquery.datepicker.extension.range.js',
		'app/libs/datepicker-ui/dist/datepicker-ru.js',
		'app/libs/magnificpopup/jquery.magnific-popup.min.js',
		'app/libs/mask-plugin/dist/jquery.mask.min.js',
		'app/libs/cropperjs/cropper.min.js',
		// 'app/libs/typeahead/dist/typeahead.bundle.min.js',
		'app/libs/fullcalendar/dist/core_main.js',
		'app/libs/fullcalendar/dist/ru.js',
		'app/libs/fullcalendar/dist/daygrid_main.js',
		])
	.pipe(concat('scripts.min.js'))
	// .pipe(uglify()) // Минимизировать весь js (на выбор)
	.pipe(gulp.dest('app/normal/js'))
	.pipe(browserSync.reload({stream: true}));
});

gulp.task('normal-browser-sync', function() {
	browserSync({
		server: {
			baseDir: 'app/normal'//Обновляет стран браузера когда компилир html
		},
		notify: false,
		// tunnel: true,
		// tunnel: "projectmane", //Demonstration page: http://projectmane.localtunnel.me
	});
});

gulp.task('normal-libs-min-css', function() {
	return gulp.src('app/sass/all.sass')
	.pipe(sass({outputStyle: 'expand'}).on("error", notify.onError()))
	.pipe(rename({suffix: '.min', prefix : ''}))
	.pipe(autoprefixer(['last 15 versions']))
	//.pipe(cleanCSS()) // Опционально, закомментировать при отладке
	.pipe(gulp.dest('app/normal/css'))
	.pipe(browserSync.reload({stream: true}));
});

gulp.task('normal-css', function() {
	return gulp.src(['app/sass/fonts.sass','app/sass/icomoon.sass', 'app/sass/main.sass'])
	.pipe(sass({outputStyle: 'expand'}).on("error", notify.onError()))
	.pipe(autoprefixer(['last 15 versions']))
	.pipe(gulp.dest('app/normal/css'))
	.pipe(browserSync.reload({stream: true}));
});

gulp.task('normal-fileinclude', function() {
  gulp.src(['app/html/*.html'])
    .pipe(fileinclude({
      prefix: '@@',
      basepath: 'app/html'
    }))
    .pipe(gulp.dest('app/normal'));
});

gulp.task('normal-img-min', function() {
	return gulp.src(['app/img/**/*','app/img/**/**/*'])
	.pipe(cache(imagemin()))
	.pipe(gulp.dest('app/normal/img')); 
});

gulp.task('normal-fonts', function() {
	return gulp.src(['app/fonts/**/*','app/fonts/**/**/*'])
	.pipe(gulp.dest('app/normal/fonts')); 
});

gulp.task('normal-watch', ['normal-fileinclude','normal-libs-min-css','normal-css','normal-js','normal-libs-js','normal-browser-sync'], function() {
	gulp.watch('app/sass/all.sass', ['normal-libs-min-css']);
	gulp.watch('app/blocks/**/*.sass', ['normal-css']);
	gulp.watch(['app/sass/fonts.sass', 'app/sass/icomoon.sass', 'app/sass/main.sass', 'app/sass/mobile-nav.sass'], ['normal-css']);
	gulp.watch('app/libs/**/*.js', ['normal-libs-js']);

	gulp.watch('app/blocks/**/*.js', ['normal-js']);
	
	gulp.watch('app/js/common.js', ['normal-js']);
	gulp.watch('app/blocks/**/*.html', ['normal-fileinclude']);
	gulp.watch('app/html/*.html', ['normal-fileinclude']);
	gulp.watch('app/normal/*.html', browserSync.reload);
});

gulp.task('normal', ['normal-watch','normal-img-min','normal-fonts']);
