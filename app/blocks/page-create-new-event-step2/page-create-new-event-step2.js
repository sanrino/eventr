
/*_______ .page-create-new-event-step2 _______*/
$( document ).ready(function() {
	var hiddenFreeEvent = function() {
		if($(this).is(':checked')){
			$('.site-link__title, .site-input, .bonus-places, .promotional-offer-hide, .wrap-content-hide').hide(500);
		}else{
			$('.site-link__title, .site-input, .bonus-places, .wrap-content-hide').show(500);
			$('.promotional-offer-hide').css('display', 'flex');
		}
	};
	var hiddenReferralProgram = function() {
		if($(this).is(':checked')){
			$('.site-link__title, .site-input, .bonus-places').show(500);
			$('.promotional-offer-hide').css('display', 'flex');
		}else{
			$('.site-link__title, .site-input, .bonus-places, .promotional-offer-hide ').hide(500);
		}
	};
	
	$('#free-event').on( 'click', hiddenFreeEvent );
	$('#referral-program').on( 'click', hiddenReferralProgram );

});
/*________End .page-create-new-event-step2 ________*/