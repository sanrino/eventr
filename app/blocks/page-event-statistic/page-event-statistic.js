
/*_______ .page-event-statistic _______*/


$(document).ready(function() {
	var $select2 = $('select.select_all_city').select2()
	$select2.next().addClass("select_all_city")
	if ($(document).find('.page-client-base').length == 0) {
		$(".mobile-main-content .event-btn-more").on('click', function(){
			$(this).closest('.event-item').addClass('selected');
			$('body').addClass('no-scroll');
		});
		$('.mobile-main-content .event-btn-back').on('click', function(){
			$(this).closest('.event-item').removeClass('selected');
			$('body').removeClass('no-scroll');
		});
	}
});


/*________End .page-event-statistic ________*/