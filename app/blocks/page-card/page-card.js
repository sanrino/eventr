/*________ page-card ________*/
$('.about-event-slider').slick({
	slidesToShow: 1,
	arrows: true,
	nextArrow: '<div class="slick-next"><img src="img/arrow-right.svg" alt="" /></div>',
	prevArrow: '<div class="slick-prev"><img src="img/arrow-left.svg" alt="" /></div>',
	dots: true,
	speed: 2000,
	autoplaySpeed: 5000,
	infinite: true,
	responsive: [
		{
			breakpoint: 767,
			settings: {
				dots: false, 
			}
		},
	]
});
$('.about-event-slider').css({
	opacity: '1',
});

$(".notification-form .btn-def-submit").on('click',  function(event) {
	event.preventDefault();
	$(this).closest(".about-event-modal").find('.notification-form').addClass('hide');
	$(this).closest(".about-event-modal").find('.notification-sent ').addClass('show');
});

$('.ticket-succes-pay').on('click',  function(event) {
	setTimeout(function(){ $('body').addClass('modal-open'); }, 500);
});
$('.btn-def-review').on('click',  function(event) {
	$(this).closest('.modal').addClass('open-share')
});

//mobile table
$('.more-inform').on('click',  function(event) {
	$('.tr').removeClass('selected');
	$(this).closest('.tr').addClass('selected');
	$('.sector-prev-arrow').addClass('show');
});
$('.sector-prev-arrow').on('click',  function(event) {
	event.preventDefault();
	$(this).removeClass('show');
	$('.tr').removeClass('selected');
});
//End mobile table

//maps search
// var myMap, myPlacemark, coords;

// ymaps.ready(init);

//     function init () {

// 		//Определяем начальные параметры карты
//         myMap = new ymaps.Map('map-search', {
//             center: [56.326944, 44.0075], 
//             zoom: 18
//         });	

// 		//Определяем элемент управления поиск по карте	
// 		var SearchControl = new ymaps.control.SearchControl({noPlacemark:true});	

// 		//Добавляем элементы управления на карту
// 		myMap.controls.add(SearchControl);

// 		coords = [56.326944, 44.0075];

// 		//Определяем метку и добавляем ее на карту				
// 		myPlacemark = new ymaps.Placemark(myMap.getCenter(), {
//         hintContent: 'A custom placemark icon',
//         balloonContent: 'This is a pretty placemark'
//     }, {
//         iconLayout: 'default#imageWithContent',
//         iconImageHref: 'img/map-marker.svg',
//         iconImageSize: [30, 42],
//         iconContentOffset: [-35, -50],
//        });

// 		myMap.geoObjects.add(myPlacemark);	

// 		// Создаем выпадающую панель с поисковыми подсказками и прикрепляем ее к HTML-элементу по его id.
// 	    var suggestView1 = new ymaps.SuggestView('suggest1', {
// 	    	results: 1,
//         	offset: [20, 30]
// 	    });


// 	    suggestView1.events.add('select', function (event) {

// 	    	current = event.get('item').value;

// 	    	geocode(current).then(
// 			    function (res) {
// 			      	var firstGeoObject = res.geoObjects.get(0);
// 			        // Координаты геообъекта.
// 			        coords = firstGeoObject.geometry.getCoordinates();
// 			        console.log("мои кординаты", coords);
// 			        currentCoords(coords);
// 			});

// 	    	function currentCoords(current) {
// 	    		myMap.panTo(coords, {
//             		flying: 8,
//             		delay: 1
//         		});

// 		    	myPlacemark = new ymaps.Placemark(current, {}, {
// 			        	iconLayout: 'default#imageWithContent',
// 			        	iconImageHref: 'img/map-marker.svg',
// 			        	iconImageSize: [30, 42],
// 			        	iconContentOffset: [-35, -50],
// 			        });
// 		    		myMap.geoObjects.add(myPlacemark);
// 		   	};

// 		});

// }

// function geocode(request) {
// 	return ymaps.geocode(request, {results: 1})
// };

//End maps search

if($('#map').length){

	ymaps.ready(function () {
	    var myMap = new ymaps.Map('map', {
	            center: [54.739331, 55.957074],
	            zoom: 14,
	            controls: []
	        }, {
	            searchControlProvider: 'yandex#search'
	        }),

	        myPlacemark = new ymaps.Placemark(myMap.getCenter(), {
	            hintContent: 'A custom placemark icon',
	            balloonContent: 'This is a pretty placemark'
	        }, {
	            iconLayout: 'default#imageWithContent',
	            iconImageHref: 'img/map-marker.svg',
	            iconImageSize: [30, 42],
	            iconContentOffset: [-35, -50],
	           });
			myMap.geoObjects.add(myPlacemark);
	});
}
/*________ End page-card ________*/
