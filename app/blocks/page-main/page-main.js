/*________ .page-main ________*/	
	//header-insert
	$(".header .catalog-part").on('click',  function(event) {
		$(".menu-categories").toggleClass('show');
	});
	//End header-insert

	$('.poster .icon-like').on('click',  function(event) {
		event.preventDefault();
		$(this).closest('.poster').toggleClass('active');
	});

	$('.slider-banner').slick({
		slidesToShow: 1,
		arrows: true,
		nextArrow: '<div class="slick-next"><img src="img/arrow-right.svg" alt="" /></div>',
  		prevArrow: '<div class="slick-prev"><img src="img/arrow-left.svg" alt="" /></div>',
		dots: true,
		//autoplay: true,
		speed: 2000,
		autoplaySpeed: 5000,
		infinite: true,
		responsive: [
			{
				breakpoint: 767,
				settings: {
					dots: false, 
				}
			},
		]
	});
	$('.posters-slider-popular').slick({
		slidesToShow: 3,
		arrows: false,
		dots: false,
		speed: 2000,
		autoplaySpeed: 5000,
		infinite: true,
		responsive: [
			{
				breakpoint: 768,
				settings: {
					slidesToShow: 2, 
					centerMode: true,
				}
			},
			{
				breakpoint: 576,
				settings: {
					slidesToShow: 1, 
					centerMode: true,
				}
			},
		]
	});
	$('.posters-slider-active').slick({
		slidesToShow: 4,
		arrows: false,
		dots: false,
		speed: 2000,
		autoplaySpeed: 5000,
		infinite: true,
		responsive: [
			{
				breakpoint: 992,
				settings: {
					slidesToShow: 3, 
				}
			},
		
			{
				breakpoint: 767,
				settings: {
					slidesToShow: 1, 
					centerMode: true,
				}
			},
		],
	});

	$('.poster-category-slider').slick({
		slidesToShow: 4,
		arrows: false,
		dots: true,
		autoplaySpeed: 5000,
		infinite: true,
		responsive: [
			{
				breakpoint: 992,
				settings: {
					slidesToShow: 3, 
				}
			},
		    {
				breakpoint: 768,
				settings: {
					slidesToShow: 2, 
					centerMode: true,
				}
			},
			{
				breakpoint: 576,
				settings: {
					slidesToShow: 1, 
					centerMode: true,
				}
			},
		]
	});
	
	$('.left').click(function(){
		console.log('left');
		$(this).closest('.row-slider').find('.posters').slick('slickPrev');
	})
	$('.right').click(function(){
		console.log('right');
		$(this).closest('.row-slider').find('.posters').slick('slickNext');
	})

	//menu-mobile-btn click show menu
	$(".menu-mobile-btn").on('click',  function(event) {
		$(".header").toggleClass('responsive');
		$("body").toggleClass('responsive');
		$("body").toggleClass('no-scroll');
	});

	$(".mobile-navigate-panel li").on('click', function(event) {
		$(".mobile-navigate-panel li").removeClass('active');
		$(this).addClass('active');
		$('.mobile-tabs .tab').removeClass('show');
		$('.mobile-tabs .tab').eq($(this).index()).addClass('show');
	});
	
	$('.menu-categories-mobile .icon-arrow').on('click',  function(event) {
		$('.submenu').removeClass('show');
		$(this).closest('.has-submenu').find('.submenu').addClass('show');
	});
	$('.category-head').on('click', function(event) {
		$(this).closest('.submenu').removeClass('show');
	});
	$(".menu-mobile-btn").on('click', function(event) {
		$(".mobile-navigate").toggleClass('show');
	});//End menu-mobile-btn click show menu

	//hide notifications footer
	$(".footer .hide-btn").on('click', function(event) {
		$(".footer-notifications").addClass("hide-alert");
		$(".footer").addClass("footer-default");
	});//End hide notifications footer

	//copy link
	function copyFunction() {
	  /* Get the text field */
	  var copyText = document.getElementById("copyText");
	  /* Select the text field */
	  copyText.select();
	  copyText.setSelectionRange(0, 99999); /*For mobile devices*/
	  /* Copy the text inside the text field */
	  document.execCommand("copy");
	}
	
	//hide-show alert copy link
	$('.btn-def-copy').on('click', function(event) {
		$('.copy-link').addClass('hide');
		$('.notification-sent').addClass('show');
	});//End hide-show alert copy link

	//typehead-desktop show
	$(".search-desktop input[type=text]").focus(function(event) {
		$(this).closest('.search-desktop').addClass('active');
		$(".typehead-desktop").addClass('show');
		$(".overlay").addClass('show');
		$("body").addClass('no-scroll');
		$('.header').addClass('no-shadow')
	});//End typehead

	//hidden typehead-desktop
	$('.icon-hide').on('click', function(event) {
		$(".overlay").removeClass('show');
		$(".typehead-desktop").removeClass('show');
		$("body").removeClass('no-scroll');
		$('.search').removeClass('active');
		$(this).closest(".search").find("input[type=text]").val(" ");
	});

	//typehead-mob-main-page show
	$(".search-mob-main-page input[type=text]").focus(function(event) {
		$(this).closest('.search-mob-main-page').addClass('active');
		$(".typehead-mob-main-page").addClass('show');
		$(".overlay").addClass('show');
		$("body").addClass('no-scroll');
	});//End typehead-mobile

	//hidden typehead-mob-main-page
	$('.icon-hide').on('click', function(event) {
		$(".typehead-mob-main-page").removeClass('show');
		$("body").removeClass('no-scroll');
		$('.search-mob-main-page').removeClass('active');
		$('.search').removeClass('active');
		$(this).closest(".search-mob-main-page").find("input[type=text]").val(" ");
	});

	//mobile vers  
	$(".loupe").on('click', function(event) {
		$('.search-mobile-fixed').addClass('show');
		$("body").addClass('no-scroll');
	});
	$('.search-mobile-fixed input[type=text]').focus(function(event) {
		$(".typehead-mobile").addClass('show');
	});
	//hidden typehead-mob-main-page
	$('.icon-hide').on('click', function(event) {
		$(this).closest(".search-mobile-fixed").find(".typehead-mobile").removeClass('show');
	});
	$(".search-panel").on('click',  function(event) {
		$(".search-mobile-fixed").removeClass('show');
		$("body").removeClass('no-scroll');
	});
/*________ End .page-main ________*/	