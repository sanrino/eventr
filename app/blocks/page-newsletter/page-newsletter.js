$(document).ready(function() {

    let arr1 = [];
    
    $('.js-add-item-more').click(function () {
        var select_1 = $("#select2-one-select-container").text();
        var select_2 = $("#select2-two-select-container").text();
        var total = arr1.push({title1:select_1, title2:select_2});

        var creation = $(this).closest('.wrap-selects').siblings(".creation")
        creation[0].innerHTML = ''
    
        arr1.forEach(el => {
            var newElems = $("<div class='item'></div>")
                .append("<div class='first'>" + el.title1 + "</div>")
                .append("<div class='next'>" + ' ' + ' (' + el.title2 + ')' + "</div>")
                .append("<div class='del-item'><div class='icon icon-close'></div></div>")
                creation.append(newElems);
        });
        $('.del-item').click(function(){
            let item = $(this).parent('.item');
            arr1.splice($(item).index(), 1);
            item.remove();
        });
    });

    let arr2 = [];

    $('.js-add-item-less').click(function () {
        var select_3 = $("#select2-three-select-container").text();
        var select_4 = $("#select2-fore-select-container").text();
        var total = arr2.push({title1:select_3, title2:select_4});
        var creation = $(this).closest('.wrap-selects').siblings(".creation")
        creation[0].innerHTML = ''

        arr2.forEach(el => {
            var newElems = $("<div class='item'></div>")
                .append("<div class='first'>" + el.title1 + "</div>")
                .append("<div class='next'>" + ' ' + ' (' + el.title2 + ')' + "</div>")
                .append("<div class='del-item'><div class='icon icon-close'></div></div>")
            creation.append(newElems);
        });
        $('.del-item').click(function () {
            let item = $(this).parent('.item');
            arr2.splice($(item).index(), 1);
            item.remove();
        });
    });

    $('.wrap-btn-back').on('click', function (event) {
        $('.filter-item-poster').removeClass('show');
    });
});