
/*_______ .page-create-new-event-step1 _______*/

	//small-calendar
	$( document ).ready(function() {

		/*______mask-timePicker_____*/
		var maskBehavior = function (val) {
			val = val.split(":");
			return (parseInt(val[0]) > 19)? "HZ:M0" : "H0:M0";
		}
	
		spOptions = {
			onKeyPress: function(val, e, field, options) {
				field.mask(maskBehavior.apply({}, arguments), options);
			},
			placeholder: "- - : - -",
			clearIfNotMatch: true,
			hourFormat: 12,
			translation: {
				'H': { pattern: /[0-2]/, optional: false },
				'Z': { pattern: /[0-3]/, optional: false },
				'M': { pattern: /[0-5]/, optional: false}
			}
		};
	
		$('.time').mask(maskBehavior, spOptions);
		/*______END mask-timePicker_____*/
		
		/*______datepicker_____*/
		var numberOfMonthsMob = function(){
			var win = $(this); //this = window
			if (win.width() <= 767) {
				$('.date_range1').datepicker({
					range: 'period', // режим - выбор периода
					numberOfMonths: 1,
					showOtherMonths  : true, //Заполнение пустых ячеек календаря датами соседних месяцев
					navigationAsDateFormat: false,
		
					onSelect: function(dateText, inst, extensionRange) {
						$('.date-range2').val(extensionRange.startDateText + ' - ' + extensionRange.endDateText);
					}
				});
			}
			if (win.width() >= 768) {
				$('.date_range1').datepicker({
					range: 'period', // режим - выбор периода
					numberOfMonths: 3,
					showOtherMonths  : true, //Заполнение пустых ячеек календаря датами соседних месяцев
					navigationAsDateFormat: false,
					onSelect: function(dateText, inst, extensionRange) {
						$('.date-range2').val(extensionRange.startDateText + ' - ' + extensionRange.endDateText);
					}
				});
			}
		}
		numberOfMonthsMob();
		/*______END datepicker_____*/
	$('.open-popup-link').magnificPopup({
		type:'inline',
		midClick: true, // Allow opening popup on middle mouse click. Always set it to true if you don't provide alternative source in href.
		callbacks: {

            beforeOpen: function () {
            		jQuery('body').css('overflow', 'hidden');
        	},
        	beforeClose: function () {
        		jQuery('body').css('overflow', 'auto');
			},
			
			close: function(){
				if ($(window).width() > 768)
					$('.date_range').val($('.date-range2').val());
			},
			open: function () {
					console.log('init');
					$(".wrap-mfp-close").show();
					$(".wrap-btn-choose").show();
					if ($(window).width() <= 768) {
						$(".date_range1").datepicker("destroy");
						$('.date_range1').datepicker({
							range: 'period', // режим - выбор периода
							numberOfMonths: 1,
							showOtherMonths: true, //Заполнение пустых ячеек календаря датами соседних месяцев
							navigationAsDateFormat: false,

							onSelect: function (dateText, inst, extensionRange) {
								$('.date-range2').val(extensionRange.startDateText + ' - ' + extensionRange.endDateText);
							}
						});
					};
					if ($(window).width() >= 768) {
						$(".date_range1").datepicker("destroy");
						$('.date_range1').datepicker({
							range: 'period', // режим - выбор периода
							numberOfMonths: 3,
							showOtherMonths: true, //Заполнение пустых ячеек календаря датами соседних месяцев
							navigationAsDateFormat: false,
							onSelect: function (dateText, inst, extensionRange) {
								$('.date-range2').val(extensionRange.startDateText + ' - ' + extensionRange.endDateText);
							}
						});
					}
			},
		}
	});
	// $('.open-popup-link').on('click', function(){
	// 	console.log('init');
	// 	$(".wrap-mfp-close").show();
	// 	$(".wrap-btn-choose").show();
	// 	if ($(window).width() <= 768) {
	// 		$(".date_range1").datepicker("destroy");
	// 		$('.date_range1').datepicker({
	// 			range: 'period', // режим - выбор периода
	// 			numberOfMonths: 1,
	// 			showOtherMonths: true, //Заполнение пустых ячеек календаря датами соседних месяцев
	// 			navigationAsDateFormat: false,

	// 			onSelect: function (dateText, inst, extensionRange) {
	// 				$('.date-range2').val(extensionRange.startDateText + ' - ' + extensionRange.endDateText);
	// 			}
	// 		});
	// 	};
	// 	if ($(window).width() >= 768) {
	// 		$(".date_range1").datepicker("destroy");
	// 		$('.date_range1').datepicker({
	// 			range: 'period', // режим - выбор периода
	// 			numberOfMonths: 3,
	// 			showOtherMonths: true, //Заполнение пустых ячеек календаря датами соседних месяцев
	// 			navigationAsDateFormat: false,
	// 			onSelect: function (dateText, inst, extensionRange) {
	// 				$('.date-range2').val(extensionRange.startDateText + ' - ' + extensionRange.endDateText);
	// 			}
	// 		});
	// 	}
	// });

	$('.date-select').on('click', function(){
		$('.date_range').val($(this).closest('.mfp-content').find('.date-range2').val());
		$.magnificPopup.close();
	});
	$('.mfp-close').on('click', function(){
		console.log('destroy');
		$( ".date_range1" ).datepicker( "destroy" );
		$.magnificPopup.close()
	});

	$(window).on('resize', function(){
		// numberOfMonthsMob();
		if ($(this).width() <= 768) {
			$( ".date_range1" ).datepicker( "destroy" );
			$('.date_range1').datepicker({
				range: 'period', // режим - выбор периода
				numberOfMonths: 1,
				showOtherMonths  : true, //Заполнение пустых ячеек календаря датами соседних месяцев
				navigationAsDateFormat: false,
	
				onSelect: function(dateText, inst, extensionRange) {
					$('.date-range2').val(extensionRange.startDateText + ' - ' + extensionRange.endDateText);
				}
			});
		};
		if ($(this).width() >= 768) {
			$( ".date_range1" ).datepicker( "destroy" );
			$('.date_range1').datepicker({
				range: 'period', // режим - выбор периода
				numberOfMonths: 3,
				showOtherMonths  : true, //Заполнение пустых ячеек календаря датами соседних месяцев
				navigationAsDateFormat: false,
				onSelect: function(dateText, inst, extensionRange) {
					$('.date-range2').val(extensionRange.startDateText + ' - ' + extensionRange.endDateText);
				}
			});
		}
		$.magnificPopup.close(); 
	});
});


//************************ Drag and drop ***************** //
if ($('.drop-area')[0]) {


	let dropArea = document.querySelector(".drop-area")

	// Prevent default drag behaviors
	;['dragenter', 'dragover', 'dragleave', 'drop'].forEach(eventName => {
		dropArea.addEventListener(eventName, preventDefaults, false)   
		document.body.addEventListener(eventName, preventDefaults, false)
	})

	// Highlight drop area when item is dragged over it
	;['dragenter', 'dragover'].forEach(eventName => {
		dropArea.addEventListener(eventName, highlight, false)
	})

	;['dragleave', 'drop'].forEach(eventName => {
		dropArea.addEventListener(eventName, unhighlight, false)
	})

	// Handle dropped files
	dropArea.addEventListener('drop', handleDrop, false)

	function preventDefaults (e) {
		e.preventDefault()
		e.stopPropagation()
	}

	function highlight(e) {
		dropArea.classList.add('highlight')
	}

	function unhighlight(e) {
		dropArea.classList.remove('active')
	}

	function handleDrop(e) {
		var dt = e.dataTransfer
		var files = dt.files

		handleFiles(files)
	}

	let uploadProgress = []
	let progressBar = document.getElementById('progress-bar')

	// function initializeProgress(numFiles) {
	// 	progressBar.value = 0
	// 	uploadProgress = []

	// 	for(let i = numFiles; i > 0; i--) {
	// 		uploadProgress.push(0)
	// 	}
	// }

	function updateProgress(fileNumber, percent) {
		// uploadProgress[fileNumber] = percent
		// let total = uploadProgress.reduce((tot, curr) => tot + curr, 0) / uploadProgress.length
		// console.debug('update', fileNumber, percent, total)
		// progressBar.value = total
	}

	function handleFiles(files, elClass) {
		files = [...files]
		// initializeProgress(files.length)
		files.forEach(uploadFile)
		files.forEach((item => {
			previewFile(item, elClass)
		}))
	}
	
	function previewFile(file, elClass) {
		console.log(file, elClass)
		const el = $(elClass + ' .small-img li .wrap-empty')
		let reader = new FileReader()
		reader.readAsDataURL(file)
		reader.onloadend = function() {
			let img = document.createElement('img');
			img.src = reader.result;
			$(el).each(function(){
				if (!$(this).hasClass('active')){
					// $(this).html('');
					$(this).html(img);
					$(this).addClass('active');
					return false;
				}
			});
		}
	}
	$(".small-img li .wrap-empty").click(function(){
		if(!$(this).hasClass("check")){
			$(".small-img li .wrap-empty").parents("li").find(".check").removeClass("check");
			if (!$(this).hasClass("active")){
				return false;
			}else{
				$(this).addClass("check");
			}
		}
	})
	$(".small-img-close").click(function(){
		$(this).parents('li').find('.wrap-empty').empty();
		$(this).parents('li').find('.wrap-empty').removeClass("active");
		$(this).parents('li').find('.wrap-empty').removeClass("check");
		$("#gallery > img").attr('src', '');
		$("#gallery > img").attr('src', $("#gallery > img").attr('data-src'));
		//Добавить проверуку на класс active и check   
	})
	$('body').on('click', '.small-img li .wrap-empty.active', function(){
		$('#gallery > img').attr('src', $(this).find('img').attr('src'));
	});

	function uploadFile(file, i) {
		var url = 'https://api.cloudinary.com/v1_1/joezimim007/image/upload'
		var xhr = new XMLHttpRequest()
		var formData = new FormData()
		xhr.open('POST', url, true) 
		// xhr.open('POST', true) 
		xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest')

		// Update progress (can be used to show progress indicator)
		xhr.upload.addEventListener("progress", function(e) {
			updateProgress(i, (e.loaded * 100.0 / e.total) || 100)
		})

		xhr.addEventListener('readystatechange', function(e) {
			if (xhr.readyState == 4 && xhr.status == 200) {
			updateProgress(i, 100) // <- Add this
			}
			else if (xhr.readyState == 4 && xhr.status != 200) {
			// Error. Inform the user
			}
		})

		formData.append('upload_preset', 'ujpu6gyk')
		formData.append('file', file)
		xhr.send(formData)
	}
}
	//******** END Drag and drop ******** //
	//******** select2 ******** //
	//select2
	// $('.select').select2({
	// 	containerCssClass: "error" 
	// });
	//******** END select2 ******** //


if($('#map-search').length){
	//maps search
	var myMap, myPlacemark, coords;

	ymaps.ready(init);

	    function init () {

			//Определяем начальные параметры карты
	        myMap = new ymaps.Map('map-search', {
	            center: [56.326944, 44.0075], 
	            zoom: 17,
	            controls: []
	        });	

			//Определяем элемент управления поиск по карте	
			var SearchControl = new ymaps.control.SearchControl({
				noPlacemark:true,
			});	

			//Добавляем элементы управления на карту
			// myMap.controls.add(SearchControl);

			coords = [56.326944, 44.0075];

			//Определяем метку и добавляем ее на карту				
			myPlacemark = new ymaps.Placemark(myMap.getCenter(), {
	        hintContent: 'A custom placemark icon',
	        balloonContent: 'This is a pretty placemark'
	    }, {
	        iconLayout: 'default#imageWithContent',
	        iconImageHref: 'img/map-marker.svg',
	        iconImageSize: [30, 42],
	        iconContentOffset: [-35, -50],
	       });

			myMap.geoObjects.add(myPlacemark);	

			// Создаем выпадающую панель с поисковыми подсказками и прикрепляем ее к HTML-элементу по его id.
		    var suggestView1 = new ymaps.SuggestView('suggest1', {
		    	results: 1,
	        	offset: [20, 30]
		    });


		    suggestView1.events.add('select', function (event) {

		    	current = event.get('item').value;

		    	geocode(current).then(
				    function (res) {
				      	var firstGeoObject = res.geoObjects.get(0);
				        // Координаты геообъекта.
				        coords = firstGeoObject.geometry.getCoordinates();
				        // console.log("мои координаты", coords);
				        currentCoords(coords);
				});

		    	function currentCoords(current) {
		    		myMap.panTo(coords, {
	            		flying: 8,
	            		delay: 1
	        		});

			    	myPlacemark = new ymaps.Placemark(current, {}, {
				        	iconLayout: 'default#imageWithContent',
				        	iconImageHref: 'img/map-marker.svg',
				        	iconImageSize: [30, 42],
				        	iconContentOffset: [-35, -50],
				        });
			    	myMap.geoObjects.add(myPlacemark);
			   	};

			});

	}

	function geocode(request) {
		return ymaps.geocode(request, {results: 1})
	};
}
//End maps search
	
/*________End .page-create-new-event-step1 ________*/