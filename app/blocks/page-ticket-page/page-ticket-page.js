$(document).ready(function() {
    function formatState (state) {
        if (!state.id) { return state.text; }
        var $state = $(
          '<span><img src="img/svg/' +  state.element.value.toLowerCase() + ' "class="img-icon" /> ' + state.text + '</span>'
        );
       var baseUrl = "img/svg";
       // Use .text() instead of HTML string concatenation to avoid script injection issues

       $state.find("span").text(state.text);
       $state.find("img").attr("src", baseUrl + "/" + state.element.value.toLowerCase());
        $('.select2-results__options').addClass('my_dropdown');
       return $state;
    };
      
    var $select2 = $(".js-example-templating").select2({
        templateResult: formatState,
        templateSelection: formatState,

    });
});