$(document).ready(function() {

    //select2
    $('.select').select2({
    });

    //calendar
    $(function() {
        $.datepicker.setDefaults($.datepicker.regional['ru']);
        $('.datepicker').datepicker({
            range: 'period', // режим - выбор периода
            numberOfMonths: 1,
            showOtherMonths  : true, //Заполнение пустых ячеек календаря датами соседних месяцев
            navigationAsDateFormat: false,
            
            showOn: "button",
            buttonImageOnly: true,
            buttonText: "Select date",
            onSelect: function(dateText, inst, extensionRange) {
                // extensionRange - объект расширения
                $('[name=startDate]').val(extensionRange.startDateText);
                $('[name=endDate]').val(extensionRange.endDateText);
            }
        });
        $.datepicker.formatDate( "dd, MM, yy" );

        // объект расширения (хранит состояние календаря)
        // var extensionRange = $('.datepicker').datepicker('widget').data('datepickerExtensionRange');
        // if(extensionRange.startDateText) $('[name=startDate]').val(extensionRange.startDateText);
        // if(extensionRange.endDateText) $('[name=endDate]').val(extensionRange.endDateText);
                
    });
    
    //noslider
    var limitSlider = document.getElementById('slider-limit');
    var limitFieldMin = document.getElementById('slider-limit-value-min');
    var limitFieldMax = document.getElementById('slider-limit-value-max');

    if (limitSlider) {  
        noUiSlider.create(limitSlider, {
                start: [3000, 15000],
                limit: 200000,
            // behaviour: 'drag',
            connect: true,
            range: {
                'min': 0,
                'max': 20000
            },
            format: {
                // 'to' the formatted value. Receives a number.
                to: function (value) {
                    return value.toFixed(0) + '<b> &#8381;</b>';
                },
                // 'from' the formatted value.
                // Receives a string, should return a number.
                from: function (value) {
                    value = (+value).toFixed(0);
                    return value;
                }
            }
        });
        limitSlider.noUiSlider.on('update', function (values, handle) {
            (handle ? limitFieldMax : limitFieldMin).innerHTML = values[handle];
        });
    }
    
    //slider-catalog
    $('.posters-slider-active-catalog').slick({
		slidesToShow: 3,
		arrows: true,
		dots: false,
		speed: 2000,
		autoplaySpeed: 5000,
		infinite: true,
		appendArrows: $(".posters-active__arrows"),
		nextArrow: '<div class="icon-arrow icon-arrow-next"><img src="img/arrow-sm-right.svg" alt="" /></div>',
		prevArrow: '<div class="icon-arrow icon-arrow-prev"><img src="img/arrow-sm-left.svg" alt="" /></div>',
		responsive: [
			{
				breakpoint: 992,
				settings: {
					slidesToShow: 2, 
				}
			},
		
			{
				breakpoint: 767,
				settings: {
					slidesToShow: 1, 
					centerMode: true,
				}
			},
		],
    });
    
    //hide notifications footer
	$(".hide-btn-catalog").on('click', function(event) {
		$(".banners-text").addClass("hide-alert");
    });//End hide notifications footer
    
    $("#js-show-filter").on('click',  function(event) {
        $(".side-bar").toggleClass('show-filter');
        var target = event.target;
        $(target).toggleClass('btn-filter-active');
        $("body").toggleClass('no-scroll');
        $('.filter-item').removeClass('show');
    });

    $('.filter-item__title').on('click',  function(event) {
        $(this).closest(".filter-item").addClass('show');
    });

    $('.wrap-btn-back').on('click',  function(event) {
        $('.filter-item').removeClass('show');
    });
});