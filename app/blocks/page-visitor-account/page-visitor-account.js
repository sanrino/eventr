$(document).ready(function(){

/*________ page-visitor-account-favorites ________*/
	$(window).resize(function(event) {
		curretnMonth();
	});

	
	$('.view-favorites .btn-def').on('click', function() {
		$('.view-favorites .btn-def').removeClass('active');
		$('.tab-content .tab').removeClass('active');
		var current = $(this).data('attr');
		$(this).addClass('active');
		$('[data-attr="'+current+'"]').addClass('active');

		calendar.render();
		eventDay();

		if($('.tab-content [data-attr="calendar"]').hasClass('active')){
			$('.calendar-nav').addClass('show');
		}else{
			$('.calendar-nav').removeClass('show');
		}
	});

	if($('#calendar').length){
		var calendarEl = document.getElementById('calendar');
	    var calendar = new FullCalendar.Calendar(calendarEl, {
			plugins: ['dayGrid'],
			locale: 'ru',
			header: {
			    left: '',
			    center: 'title',
			    right: ''
	  		},
	  		eventLimit: true,
	  		views: {
			    timeGrid: {
			      eventLimit: 3 // adjust to 6 only for timeGridWeek/timeGridDay
			    }
	  		},

	  		events: [
		  		{
					// url: 'http://google.com/',
					start: '2019-11-01',
					title: '$ICON',
					img: 'icon-like event-1',
		  		},
		  		{
					// url: 'http://google.com/',
					start: '2019-10-31',
					title: '$ICON',
					img: 'icon-like event-1',
		  		},
		  		{
					// url: 'http://google.com/',
					start: '2019-10-31',
					title: '$ICON',
					img: 'icon-like event-2',
		  		},
		  		{
					// url: 'http://google.com/',
					start: '2019-10-31',
					title: '$ICON',
					img: 'icon-like event-3',
		  		}

	        ],

	        eventRender: function(info,element) {
	  			info.el.innerHTML = info.el.innerHTML.replace('$ICON', "<i class='icon "+info.event.extendedProps.img+"' data-toggle='modal' data-target='#bonuses-modal'></i>");
			},
	    });

	    calendar.render();

	    document.getElementById('prev').addEventListener('click', function() {
	    	calendar.prev(); // call method
	    	curretnMonth();
	    	eventDay();
	  	});

	  	document.getElementById('next').addEventListener('click', function() {
	    	calendar.next(); // call method
	    	curretnMonth();
	    	eventDay();
	  	});
	}

  	function curretnMonth() {
  		var monthCalendar = $('.fc-header-toolbar h2').text();
  		$('#month-calendar').text(monthCalendar);
  	}
  	
  	curretnMonth();

	function eventDay(){

		//total events in day
		var totalEventsTxt = $('.fc-more').text();
		var totalEventsNum = parseInt(totalEventsTxt.replace(/\D+/g,""));
  		$('.fc-more').text(totalEventsNum);
  		//End total events in day

	  	$('.fc-today').closest('.fc-row').addClass('fc-row-today');
		if($('.fc-row-today')){
			var currentDay, indCurrentDay;

			$('.fc-row-today .fc-bg td').each(function(index, item){
				if($(this).hasClass('fc-today')){
					indCurrentDay = index;
				}
			});

			$('.fc-row-today .fc-content-skeleton tbody td').each(function(index, item){
				if(index === indCurrentDay){
					$(this).addClass('today-containe')
				}
			});
		}
	}
/*________ End page-visitor-account-favorites ________*/

});	